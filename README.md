Pricing Plans and Promotions Use cases:

1.Creating a Scheme(Set of plans).
2.Creating a Plan and adding to a scheme.
3.Creating a Free trial and adding to a scheme.
4.Adding a scheme to zip codes.
5.Creating a Promo code which can be used only once by a user and having expiry date.
6.Randomly picking a scheme from the schemes alloted to zip code of user.
7.Displaying the price after entering the Promo code.
8.Subscribing for a plan or a free trial.
9.Auto renewal of the plan on reaching the renewal date and Upgrading the free trial to the plan on reaching free trial expiry date.
