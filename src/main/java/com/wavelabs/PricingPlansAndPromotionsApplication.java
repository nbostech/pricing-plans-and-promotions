package com.wavelabs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@SpringBootApplication
@ComponentScan({
		"com.wavelabs.dao, com.wavelabs.model, com.wavelabs, com.wavelabs.service, com.wavelabs.resource, com.wavelabs.helper" })
@EnableSwagger2
public class PricingPlansAndPromotionsApplication {
	public static void main(String[] args) {
		SpringApplication.run(PricingPlansAndPromotionsApplication.class, args);
	}
}
