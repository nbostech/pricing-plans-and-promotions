package com.wavelabs.model;

public class Plan {
	private int id;
	private String name;
	private double price;
	private int durationInMonths;
	private int noOfDaysForFreeTrial;

	public Plan() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getDurationInMonths() {
		return durationInMonths;
	}

	public void setDurationInMonths(int durationInMonths) {
		this.durationInMonths = durationInMonths;
	}

	public int getNoOfDaysForFreeTrial() {
		return noOfDaysForFreeTrial;
	}

	public void setNoOfDaysForFreeTrial(int noOfDaysForFreeTrial) {
		this.noOfDaysForFreeTrial = noOfDaysForFreeTrial;
	}
}
