package com.wavelabs.model;

import java.util.Date;
import java.util.Set;

public class Member {
	private int id;
	private String email;
	private Plan plan;
	private Date planRenewalDate;
	private Plan nextSubscriptionPlan;
	private Date freeTrialExpiryDate;
	private double amountPaidForPresentPlan;
	private PromoCode freeTrialPromoCode;
	private Set<PromoCode> usedPromoCodes;

	public Member() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Plan getPlan() {
		return plan;
	}

	public Plan getNextSubscriptionPlan() {
		return nextSubscriptionPlan;
	}

	public void setNextSubscriptionPlan(Plan nextSubscriptionPlan) {
		this.nextSubscriptionPlan = nextSubscriptionPlan;
	}

	public void setPlan(Plan plan) {
		this.plan = plan;
	}

	public Date getPlanRenewalDate() {
		return planRenewalDate;
	}

	public void setPlanRenewalDate(Date planRenewalDate) {
		this.planRenewalDate = planRenewalDate;
	}

	public Date getFreeTrialExpiryDate() {
		return freeTrialExpiryDate;
	}

	public void setFreeTrialExpiryDate(Date freeTrialExpiryDate) {
		this.freeTrialExpiryDate = freeTrialExpiryDate;
	}

	public double getAmountPaidForPresentPlan() {
		return amountPaidForPresentPlan;
	}

	public void setAmountPaidForPresentPlan(double amountPaidForPresentPlan) {
		this.amountPaidForPresentPlan = amountPaidForPresentPlan;
	}

	public Set<PromoCode> getUsedPromoCodes() {
		return usedPromoCodes;
	}

	public void setUsedPromoCodes(Set<PromoCode> usedPromoCodes) {
		this.usedPromoCodes = usedPromoCodes;
	}

	public PromoCode getFreeTrialPromoCode() {
		return freeTrialPromoCode;
	}

	public void setFreeTrialPromoCode(PromoCode freeTrialPromoCode) {
		this.freeTrialPromoCode = freeTrialPromoCode;
	}

}
