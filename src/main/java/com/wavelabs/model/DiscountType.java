package com.wavelabs.model;

public enum DiscountType {
	PERCENTAGE, AMOUNT;
}
