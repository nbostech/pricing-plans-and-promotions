package com.wavelabs.model;

import java.util.Set;


public class ZipCode {
	private int id;
	private String zipCode;
	private String city;
	private String state;
	private Set<Scheme> schemes;
	
	public ZipCode() {

	}
	
	public Set<Scheme> getSchemes() {
		return schemes;
	}

	public void setSchemes(Set<Scheme> schemes) {
		this.schemes = schemes;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
}
