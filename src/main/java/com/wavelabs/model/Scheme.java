package com.wavelabs.model;

import java.util.Set;

public class Scheme {
	private int id;
	private String name;
	private byte isActive;
	private byte freeTrial;
	private Set<Plan> plans;

	public Scheme() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public byte getIsActive() {
		return isActive;
	}

	public void setIsActive(byte isActive) {
		this.isActive = isActive;
	}

	public byte getFreeTrial() {
		return freeTrial;
	}

	public void setFreeTrial(byte freeTrial) {
		this.freeTrial = freeTrial;
	}

	public Set<Plan> getPlans() {
		return plans;
	}

	public void setPlans(Set<Plan> plans) {
		this.plans = plans;
	}

}
