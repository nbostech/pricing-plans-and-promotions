package com.wavelabs.dao;

import org.hibernate.Session;
import org.springframework.stereotype.Component;

import com.wavelabs.model.ZipCode;
import com.wavelabs.utility.Helper;

@Component
public class ZipCodeDao {
	
	public ZipCode persistZipCode(ZipCode zipCode){
		Session session = Helper.getSession();
		session.save(zipCode);
		session.beginTransaction().commit();
		session.close();
		return zipCode;
	}
	
	public ZipCode getZipCode(int zipCodeId){
		Session session = Helper.getSession();
		ZipCode zipCode = session.get(ZipCode.class, zipCodeId);
		session.close();
		return zipCode;
	}
	
	public ZipCode updateZipCode(ZipCode zipCode){
		Session session = Helper.getSession();
		session.saveOrUpdate(zipCode);
		session.beginTransaction().commit();
		session.close();
		return zipCode;
		
	}
}
