package com.wavelabs.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Component;

import com.wavelabs.model.Member;
import com.wavelabs.utility.Helper;

@Component
public class MemberDao {

	public Member persistMember(Member member) {
		Session session = Helper.getSession();
		session.save(member);
		Transaction transaction = session.beginTransaction();
		transaction.commit();
		session.close();
		return member;

	}

	public Member getMember(int memberId) {
		Session session = Helper.getSession();
		Member member = session.get(Member.class, memberId);
		session.close();
		return member;
	}

	public Member deleteMember(int memberId) {
		Session session = Helper.getSession();
		Member member = session.get(Member.class, memberId);
		session.delete(member);
		Transaction transaction = session.beginTransaction();
		transaction.commit();
		session.close();
		return member;
	}

	public Member updateMember(Member member) {
		Session session = Helper.getSession();
		session.saveOrUpdate(member);
		Transaction transaction = session.beginTransaction();
		transaction.commit();
		session.close();
		return member;
	}
}
