package com.wavelabs.dao;

import java.util.Set;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wavelabs.service.SchemeService;
import com.wavelabs.model.Plan;
import com.wavelabs.model.Scheme;
import com.wavelabs.utility.Helper;

@Component
public class PlanDao {

	@Autowired
	SchemeService schemeService;

	public Plan persistPlan(Plan plan, int schemeId) {
		Session session = Helper.getSession();
		Scheme scheme = schemeService.getScheme(schemeId);
		Set<Plan> plans = scheme.getPlans();
		plans.add(plan);
		session.save(scheme);
		session.beginTransaction().commit();
		session.close();
		return plan;
	}

	public Plan getPlan(int planId) {
		Session session = Helper.getSession();
		Plan plan = session.get(Plan.class, planId);
		session.close();
		return plan;
	}

	public void deletePlan(int planId) {
		Session session = Helper.getSession();
		Plan plan = session.get(Plan.class, planId);
		session.delete(plan);
		session.beginTransaction().commit();
		session.close();
	}
}
