package com.wavelabs.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wavelabs.dao.PlanDao;
import com.wavelabs.model.Member;
import com.wavelabs.model.Plan;
import com.wavelabs.model.PromoCode;
import com.wavelabs.model.Scheme;
import com.wavelabs.utility.Helper;

@Component
public class PlanService {

	@Autowired
	PlanDao planDao;

	@Autowired
	PlanService planService;

	@Autowired
	MemberService memberService;

	@Autowired
	PromocodeService promoCodeService;

	public Plan addPlan(Plan plan, int schemeId) {
		return planDao.persistPlan(plan, schemeId);
	}

	public Plan getPlan(int planId) {
		return planDao.getPlan(planId);
	}

	public void deletePlan(int planId) {
		planDao.deletePlan(planId);
	}

	public boolean isPlan(int planId) {
		Plan plan = planService.getPlan(planId);
		if (plan.getPrice() <= 0 && plan.getDurationInMonths() == 0) {
			return false;
		} else {
			return true;
		}
	}

	public Scheme getScheme(int planId) {
		Session session = Helper.getSession();
		Query parent = session.createQuery("select s from Scheme s join s.plans p where p.id=:planId");
		parent.setParameter("planId", planId);
		Scheme scheme = (Scheme) parent.uniqueResult();
		session.close();
		return scheme;
	}

	public Plan getPlanAfterFreeTrial(int planId) {
		Session session = Helper.getSession();
		if (!(isPlan(planId))) {
			int schemeId = getScheme(planId).getId();
			Query lessPricePlan = session
					.createQuery("select p from Scheme s inner join s.plans p where s.id=:schemeId ORDER BY p.price");
			lessPricePlan.setParameter("schemeId", schemeId);
			lessPricePlan.setFirstResult(1);
			lessPricePlan.setMaxResults(1);
			Plan plan = (Plan) lessPricePlan.uniqueResult();
			session.close();
			return plan;
		} else {
			session.close();
			return null;
		}

	}

	public Member freeTrial(int schemeId, int planId, int memberId, String code) {
		Session session = Helper.getSession();
		Query codeQuery = session.createQuery("select p.id from PromoCode p where p.code=:code");
		codeQuery.setParameter("code", code);
		int promoCodeId = (int) codeQuery.uniqueResult();
		PromoCode promoCode = promoCodeService.getPromoCode(promoCodeId);
		Plan plan = planService.getPlan(planId);
		Plan nextPlan = getPlanAfterFreeTrial(planId);
		int noOfDays = plan.getNoOfDaysForFreeTrial();
		Date expiryDate = DateService.addDaysToDate(noOfDays);
		Member member = memberService.getMember(memberId);
		member.setPlan(plan);
		member.setNextSubscriptionPlan(nextPlan);
		member.setFreeTrialExpiryDate(expiryDate);
		member.setFreeTrialPromoCode(promoCode);
		memberService.updateMember(member);
		session.close();
		return member;
	}

	public Member makePayment(double amount, int memberId, int promoCodeId, int planId, int schemeId) {
		PromoCode promoCode = promoCodeService.getPromoCode(promoCodeId);
		String code = promoCode.getCode();
		Member member = memberService.getMember(memberId);
		Object[] amountCalculated = promoCodeService.getAmount(planId, code, memberId);
		double billAmount = (double) amountCalculated[0];
		if (billAmount > 0) {
			Plan plan = planService.getPlan(planId);
			if (amount >= billAmount) {
				member.setPlan(plan);
				int noOfDays = plan.getDurationInMonths() * 30;
				Date planRenewalDate = DateService.addDaysToDate(noOfDays);
				member.setPlanRenewalDate(planRenewalDate);
				member.setAmountPaidForPresentPlan(amount);
				Set<PromoCode> promoCodes = member.getUsedPromoCodes();
				promoCodes.add(promoCode);
				memberService.updateMember(member);
			} else {
				System.out.println("Transaction Failed!");
			}
		} else {
			System.out.println("Something went wrong!");
		}
		return member;
	}

	public Member cancelPlan(int memberId) {
		Member member = memberService.getMember(memberId);
		Plan plan = member.getPlan();
		int planId = plan.getId();
		if (isPlan(planId)) {
			member.setPlan(null);
			member.setPlanRenewalDate(null);
			member.setAmountPaidForPresentPlan(0);
			memberService.updateMember(member);
			return member;
		} else {
			member.setPlan(null);
			member.setNextSubscriptionPlan(null);
			member.setFreeTrialExpiryDate(null);
			member.setFreeTrialPromoCode(null);
			memberService.updateMember(member);
			return member;
		}
	}

	public Member changePlan(int memberId, int planId) {
		Member member = memberService.getMember(memberId);
		Plan plan = planService.getPlan(planId);
		int noOfDays = plan.getDurationInMonths() * 30;
		Date newPlanRenewalDate = DateService.addDaysToDate(noOfDays);
		member.setPlan(plan);
		member.setPlanRenewalDate(newPlanRenewalDate);
		member.setAmountPaidForPresentPlan(plan.getPrice());
		memberService.updateMember(member);
		return member;

	}

	public void renewalPlan() {
		Session session = Helper.getSession();
		@SuppressWarnings("unchecked")
		List<Member> members = session.createQuery("from Member").list();
		session.close();
		for (Member member : members) {
			if (member.getPlan() != null) {
				Plan plan = member.getPlan();
				int planId = plan.getId();
				Date today = Calendar.getInstance().getTime();
				if (isPlan(planId)) {
					Date planRenewalDate = member.getPlanRenewalDate();
					if (today.after(planRenewalDate)) {
						int noOfDays = plan.getDurationInMonths() * 30;
						Date newPlanRenewalDate = DateService.addDaysToDate(noOfDays);
						member.setPlanRenewalDate(newPlanRenewalDate);
						memberService.updateMember(member);
					}
				} else {
					Date expiry = member.getFreeTrialExpiryDate();
					if (today.after(expiry)) {
						Plan nextPlan = member.getNextSubscriptionPlan();
						PromoCode promoCode = member.getFreeTrialPromoCode();
						String code = promoCode.getCode();
						Object[] amountCalculated = promoCodeService.getAmount(nextPlan.getId(), code, member.getId());
						double price = (double) amountCalculated[0];
						if (price > 0) {
							member.setFreeTrialExpiryDate(null);
							member.setPlan(nextPlan);
							member.setNextSubscriptionPlan(null);
							member.setFreeTrialPromoCode(null);
							int noOfDays = nextPlan.getDurationInMonths() * 30;
							Date planRenewalDate = DateService.addDaysToDate(noOfDays);
							member.setPlanRenewalDate(planRenewalDate);
							member.setAmountPaidForPresentPlan(price);
							memberService.updateMember(member);
						} else {
							System.out.println("Something has gone wrong!");
						}

					}
				}
			}
		}
	}
}
