package com.wavelabs.service;

import java.util.Calendar;
import java.util.Date;

public class DateService {

	private DateService() {

	}

	public static Date addDaysToDate(int noOfDays) {
		Date today = Calendar.getInstance().getTime();
		Calendar cal = Calendar.getInstance();
		cal.setTime(today);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		cal.add(Calendar.DATE, noOfDays);
		return cal.getTime();
	}
}
