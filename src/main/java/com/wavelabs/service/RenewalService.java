package com.wavelabs.service;

import java.util.Timer;
import java.util.TimerTask;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RenewalService extends TimerTask {
	
	@Autowired
	PlanService planService;

	Timer timer;

	public RenewalService() {

	}

	public RenewalService(Timer timer) {
		this.timer = timer;
	}

	public void run() {
		planService.renewalPlan();
		timer.cancel();
	}

}
