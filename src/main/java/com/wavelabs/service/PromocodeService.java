package com.wavelabs.service;

import java.util.Calendar;
import java.util.Date;
import java.util.Set;
import java.util.logging.Logger;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wavelabs.dao.PromoCodeDao;
import com.wavelabs.model.DiscountType;
import com.wavelabs.model.Member;
import com.wavelabs.model.Plan;
import com.wavelabs.model.PromoCode;
import com.wavelabs.utility.Helper;

@Component
public class PromocodeService {
	
	@Autowired
	PromoCodeDao promoCodeDao;
	
	@Autowired
	MemberService memberService;
	
	
	@Autowired
	PlanService planService;
	
	private final static Logger LOGGER = Logger.getLogger(PromocodeService.class.getName());

	public PromoCode addPromoCode(PromoCode promoCode) {
		return promoCodeDao.persistPromoCode(promoCode);
	}

	public PromoCode getPromoCode(int promoCodeId) {
		return promoCodeDao.getPromoCode(promoCodeId);
	}

	public PromoCode updatePromoCode(PromoCode promoCode) {
		return promoCodeDao.updatePromoCode(promoCode);
	}

	public Object[] getAmount(int planId, String promocode, int memberId) {
		Session session = Helper.getSession();
		double amount = 0;
		int error;
		PromoCode promo;
		Query promoQuery = session.createQuery("from PromoCode p where p.code=:promocode");
		promoQuery.setParameter("promocode", promocode);
		promoQuery.setMaxResults(1);
		promo = (PromoCode) promoQuery.uniqueResult();
		session.close();
		if (promo != null) {
			Date expiryDate = promo.getExpiresOn();
			Date date = Calendar.getInstance().getTime();
			DiscountType discountType = promo.getDiscountType();
			Set<Integer> months = promo.getNoOfMonthsApplicable();
			double discount = promo.getDiscountAmount();
			if (expiryDate.after(date)) {
				Member member = memberService.getMember(memberId);
				Set<PromoCode> promoCodes = member.getUsedPromoCodes();
				if (!(promoCodes.contains(promo))) {
					Plan plan;
					if (planService.isPlan(planId)) {
						plan = planService.getPlan(planId);
					} else {
						plan = planService.getPlanAfterFreeTrial(planId);
					}
					double price = plan.getPrice();
					Integer planMonths = plan.getDurationInMonths();
					if (months.contains(planMonths)) {
						if (discountType.equals(DiscountType.PERCENTAGE)) {
							amount = price - ((discount / 100) * price);
						} else {
							amount = price - discount;
						}
						LOGGER.info("The amount to pay: " + amount);
						error = 0;
						return new Object[] { amount, error };

					} else {
						LOGGER.info("The Promo code you have entered is not applicable for your Plan!");
						error = 1;
						return new Object[] { amount, error };
					}

				} else {
					LOGGER.info("Sorry you have already used this promo code!");
					error = 2;
					return new Object[] { amount, error };
				}
			} else {
				LOGGER.info("The Promo code you have entered is expired!");
				error = 3;
				return new Object[] { amount, error };
			}
		} else {
			LOGGER.info("The Promo Code you have entered doesn't exist!");
			error = 4;
			return new Object[] { amount, error };
		}

	}

}
