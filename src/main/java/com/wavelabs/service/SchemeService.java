package com.wavelabs.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wavelabs.dao.SchemeDao;
import com.wavelabs.model.Scheme;
import com.wavelabs.model.ZipCode;

@Component
public class SchemeService {
	
	@Autowired
	ZipCodeService zipCodeService;
	
	@Autowired
	SchemeDao schemeDao;

	public Scheme addScheme(Scheme scheme) {
		return schemeDao.persistScheme(scheme);
	}

	public Scheme getScheme(int schemeId) {
		return schemeDao.getScheme(schemeId);
	}

	public Scheme getRandomScheme(int zipCodeId) {
		ZipCode zip = zipCodeService.getZipCode(zipCodeId);
		Set<Scheme> schemes = zip.getSchemes();
		List<Scheme> list = new ArrayList<Scheme>();
		list.addAll(schemes);
		Collections.shuffle(list);
		return list.get(0);
	}
}
