package com.wavelabs.service;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wavelabs.dao.ZipCodeDao;
import com.wavelabs.model.Scheme;
import com.wavelabs.model.ZipCode;

@Component
public class ZipCodeService {
	
	@Autowired
	ZipCodeDao zipCodeDao;
	
	@Autowired
	SchemeService schemeService;
	
	public ZipCode addZipCode(ZipCode zipCode) {
		return zipCodeDao.persistZipCode(zipCode);
	}

	public ZipCode getZipCode(int zipCodeId) {
		return zipCodeDao.getZipCode(zipCodeId);
	}

	public ZipCode addSchemeToZip(int schemeId, int zipCodeId) {
		ZipCode zipCode =getZipCode(zipCodeId);
		Scheme scheme = schemeService.getScheme(schemeId);
		Set<Scheme> schemes = zipCode.getSchemes();
		schemes.add(scheme);
		return zipCodeDao.updateZipCode(zipCode);
	}

}
