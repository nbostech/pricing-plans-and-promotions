package com.wavelabs.service;

import java.util.Calendar;
import java.util.Date;
import java.util.Timer;

public class CronJob {
	public static void main(String[] args) {
		Timer timer = new Timer();
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 00);
		calendar.set(Calendar.MINUTE, 00);
		calendar.set(Calendar.SECOND, 00);
		Date time = calendar.getTime();
		timer.schedule(new RenewalService(timer), time);
	}
}