package com.wavelabs.resource;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.service.SchemeService;
import com.wavelabs.model.Message;
import com.wavelabs.model.Scheme;

@RestController
@Component
public class SchemeResource {

	@Autowired
	SchemeService schemeService;
	private final static Logger LOGGER = Logger.getLogger(SchemeResource.class.getName());

	@RequestMapping(value = "schemes/{id}", produces = MediaType.APPLICATION_JSON_VALUE)

	public ResponseEntity<Object> getScheme(@PathVariable("id") int schemeId) {
		Scheme scheme = schemeService.getScheme(schemeId);
		if (scheme != null) {
			return ResponseEntity.status(200).body(scheme);
		} else {
			Message message = new Message();
			message.setId(404);
			message.setMessage("Scheme not found");
			return ResponseEntity.status(404).body(message);
		}
	}

	@RequestMapping(value = "/schemes", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_HTML_VALUE }, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Scheme> addScheme(@RequestBody Scheme scheme) {
		return ResponseEntity.status(200).body(schemeService.addScheme(scheme));

	}

	@RequestMapping(value = "/schemes/randomscheme", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getRandomScheme(@RequestParam("zipCodeId") int zipCodeId) {
		try {
			Scheme scheme = schemeService.getRandomScheme(zipCodeId);
			return ResponseEntity.status(200).body(scheme);

		} catch (NullPointerException ne) {
			LOGGER.log(Level.SEVERE, "Exception: ", ne);
			Message message = new Message();
			message.setId(404);
			message.setMessage("Zip Code Not Found!");
			return ResponseEntity.status(404).body(message);
		}
	}

}
