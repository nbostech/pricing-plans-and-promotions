package com.wavelabs.resource;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.service.PlanService;
import com.wavelabs.model.Member;
import com.wavelabs.model.Message;
import com.wavelabs.model.Plan;

@RestController
@Component
public class PlanResource {

	@Autowired
	PlanService planService;
	private final static Logger LOGGER = Logger.getLogger(PlanResource.class.getName());

	@RequestMapping(value = "plans/{id}", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_HTML_VALUE })
	public ResponseEntity<Object> getPlan(@PathVariable("id") int id) {
		Plan plan = planService.getPlan(id);
		if (plan != null) {
			return ResponseEntity.status(200).body(plan);
		} else {
			Message message = new Message();
			message.setId(404);
			message.setMessage("Plan not found");
			return ResponseEntity.status(404).body(message);
		}
	}

	@RequestMapping(value = "plans/planafterfreetrial/{id}", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_HTML_VALUE })
	public ResponseEntity<Object> getPlanAfterFreeTrial(@PathVariable("id") int id) {
		Plan plan = null;
		try {
			plan = planService.getPlanAfterFreeTrial(id);
		} catch (NullPointerException e) {
			LOGGER.log(Level.SEVERE, "Exception: ", e);
			Message message = new Message();
			message.setId(404);
			message.setMessage("Free Trial Not Found!");
			return ResponseEntity.status(404).body(message);
		}
		if (plan != null) {
			return ResponseEntity.status(200).body(plan);
		} else {
			Message message = new Message();
			message.setId(404);
			message.setMessage("Free Trial Not Found!");
			return ResponseEntity.status(404).body(message);
		}
	}

	@RequestMapping(value = "/plans", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_HTML_VALUE }, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Plan> addPlan(@RequestBody Plan plan, @RequestParam("schemeId") int schemeId) {
		return ResponseEntity.status(200).body(planService.addPlan(plan, schemeId));
	}

	@RequestMapping(value = "/plans/{id}", method = RequestMethod.DELETE)
	public String deletePlan(@PathVariable("id") int id) {
		planService.deletePlan(id);
		return "Plan Successfully deleted";
	}

	@RequestMapping(value = "/plans/cancelplan", method = RequestMethod.PUT, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<Member> cancelPlan(@RequestParam("memberId") int memberId) {
		return ResponseEntity.status(200).body(planService.cancelPlan(memberId));

	}

	@RequestMapping(value = "/plans/changeplan", method = RequestMethod.PUT, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<Member> changePlan(@RequestParam("memberId") int memberId,
			@RequestParam("planId") int planId) {
		return ResponseEntity.status(200).body(planService.changePlan(memberId, planId));
	}

	@RequestMapping(value = "/plans/freetrialsubscription", method = RequestMethod.PUT, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<Member> subscribeFreeTrial(@RequestParam("schemeId") int schemeId,
			@RequestParam("planId") int planId, @RequestParam("memberId") int memberId,
			@RequestParam("promoCode") String promoCode) {
		return ResponseEntity.status(200).body(planService.freeTrial(schemeId, planId, memberId, promoCode));
	}

	@RequestMapping(value = "/plans/plansubscription", method = RequestMethod.PUT, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<Member> makePayment(@RequestParam("amount") double amount,
			@RequestParam("memberId") int memberId, @RequestParam("promoId") int promoId,
			@RequestParam("planId") int planId, @RequestParam("schemeId") int schemeId) {
		return ResponseEntity.status(200).body(planService.makePayment(amount, memberId, promoId, planId, schemeId));
	}

	@RequestMapping(value = "/plans/renewal", method = RequestMethod.PUT, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<Message> renewalPlan() {
		planService.renewalPlan();
		Message message = new Message();
		message.setId(200);
		message.setMessage("Renewal Successful");
		return ResponseEntity.status(200).body(message);
	}
}
