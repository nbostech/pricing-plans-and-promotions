package com.wavelabs.resource;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.service.PromocodeService;
import com.wavelabs.model.Message;
import com.wavelabs.model.PromoCode;

@RestController
@Component
public class PromocodeResource {

	@Autowired
	PromocodeService promoCodeService;

	private final static Logger LOGGER = Logger.getLogger(PromocodeResource.class.getName());

	@RequestMapping(value = "promocodes/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getPromoCode(@PathVariable("id") int id) {
		PromoCode promoCode = promoCodeService.getPromoCode(id);
		if (promoCode != null) {
			return ResponseEntity.status(200).body(promoCode);
		} else {
			Message message = new Message();
			message.setId(404);
			message.setMessage("Promo Code not found");
			return ResponseEntity.status(404).body(message);
		}
	}

	@RequestMapping(value = "/promocodes", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_HTML_VALUE }, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PromoCode> addPromoCode(@RequestBody PromoCode code) {
		return ResponseEntity.status(200).body(promoCodeService.addPromoCode(code));
	}

	@RequestMapping(value = "promocodes/getamount", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Message> getAmount(@RequestParam("planId") int planId,
			@RequestParam("promocode") String promocode, @RequestParam("memberId") int memberId) {
		try {

			Object[] result = promoCodeService.getAmount(planId, promocode, memberId);
			double amount = (double) result[0];
			int error = (int) result[1];
			if (error == 0) {
				Message message = new Message();
				message.setId(200);
				message.setMessage("The amount you have to pay: Rs." + amount);
				return ResponseEntity.status(200).body(message);
			} else if (error == 1) {
				Message message = new Message();
				message.setId(200);
				message.setMessage("The Promo code you have entered is not applicable for your Plan!");
				return ResponseEntity.status(200).body(message);
			} else if (error == 2) {
				Message message = new Message();
				message.setId(200);
				message.setMessage("Sorry you have already used this promo code!");
				return ResponseEntity.status(200).body(message);
			} else if (error == 3) {
				Message message = new Message();
				message.setId(200);
				message.setMessage("The Promo code you have entered is expired!");
				return ResponseEntity.status(200).body(message);
			} else {
				Message message = new Message();
				message.setId(200);
				message.setMessage("The Promo Code you have entered doesn't exist!");
				return ResponseEntity.status(200).body(message);
			}

		} catch (NullPointerException ne) {
			LOGGER.log(Level.SEVERE, "Exception: ", ne);
			Message message = new Message();
			message.setId(404);
			message.setMessage("Please Check the Promo Code You Entered!");
			return ResponseEntity.status(404).body(message);
		}
	}

}
