package com.wavelabs.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.service.ZipCodeService;
import com.wavelabs.model.Message;
import com.wavelabs.model.ZipCode;

@RestController
@Component
public class ZipCodeResource {

	@Autowired
	ZipCodeService zip;

	@RequestMapping(value = "/zipcodes/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<Object> getZipCode(@PathVariable("id") int id) {
		ZipCode zipCode = zip.getZipCode(id);
		if (zipCode != null) {
			return ResponseEntity.status(200).body(zipCode);
		} else {
			Message message = new Message();
			message.setId(404);
			message.setMessage("Zip Code not found");
			return ResponseEntity.status(404).body(message);

		}
	}

	@RequestMapping(value = "/zipcodes", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_HTML_VALUE }, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ZipCode> addZipCode(@RequestBody ZipCode zipCode) {
		return ResponseEntity.status(200).body(zip.addZipCode(zipCode));

	}

	@RequestMapping(value = "/zipcodes/assignscheme", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_HTML_VALUE }, method = RequestMethod.PUT)
	public ResponseEntity<ZipCode> addSchemeToZip(@RequestParam("schemeId") int schemeId,
			@RequestParam("zipCodeId") int zipCodeId) {
		return ResponseEntity.status(200).body(zip.addSchemeToZip(schemeId, zipCodeId));

	}
}
