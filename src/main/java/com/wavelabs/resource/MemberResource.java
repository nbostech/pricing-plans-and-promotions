package com.wavelabs.resource;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.service.MemberService;
import com.wavelabs.model.Member;
import com.wavelabs.model.Message;

@RestController
@Component
public class MemberResource {

	@Autowired
	MemberService memberService;
	
	private final static Logger LOGGER = Logger.getLogger(MemberResource.class.getName());

	@RequestMapping(value = "/members", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Member> addMember(@RequestBody Member member) {
		return ResponseEntity.status(200).body(memberService.addMember(member));

	}

	@RequestMapping(value = "/members/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<Object> getMember(@PathVariable("id") int id) {
		Member member = memberService.getMember(id);
		if (member != null) {
			return ResponseEntity.status(200).body(member);

		} else {
			Message message = new Message();
			message.setId(404);
			message.setMessage("Member not found");
			return ResponseEntity.status(404).body(message);

		}
	}

	@RequestMapping(value = "/members/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Message> deleteMember(@PathVariable("id") int memberId) {
		try {
			memberService.deleteMember(memberId);
			Message message = new Message();
			message.setId(200);
			message.setMessage("Member deleted successfully!");
			return ResponseEntity.status(200).body(message);

		} catch (IllegalArgumentException exception) {
			LOGGER.log(Level.SEVERE, "Exception: ", exception);
			Message message = new Message();
			message.setId(404);
			message.setMessage("Member not found");
			return ResponseEntity.status(404).body(message);

		}
	}
}
