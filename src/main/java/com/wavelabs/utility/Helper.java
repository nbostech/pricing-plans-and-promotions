	package com.wavelabs.utility;
	
	import java.util.logging.Level;
	import java.util.logging.Logger;
	
	import org.hibernate.Session;
	import org.hibernate.SessionFactory;
	import org.hibernate.cfg.Configuration;
	
	/**
	 * Helper class is a utility class that provides objects of Configuration,
	 * Session and SessionFactory
	 * 
	 * @author muralikrishnak
	 *
	 */
	public class Helper {
	
		private final static Logger LOGGER = Logger.getLogger(Helper.class.getName());
		private static Configuration cfg = null;
		private static SessionFactory factory = null;
		private static Session session = null;
		private static int count = 0;
	
		/**
		 * This method provides only one SessionFactory, If SessionFactory is close
		 * it never creates new SessionFactory
		 * 
		 * @return SessionFactory
		 */
	
		public static SessionFactory getSessionFactory() {
			if (count == 0)
				try {
					buildSessionFactory();
				} catch (Exception e) {
					LOGGER.log(Level.SEVERE, "Exception: ", e);
				}
			return factory;
		}
	
		private static void buildSessionFactory() {
			try {
				cfg = new Configuration().configure();
				cfg =cfg.setProperty("hibernate.connection.url", System.getenv("database.url"));
				factory = cfg.buildSessionFactory();
				session = factory.openSession();
				count++;
			} catch (Exception e) {
				LOGGER.log(Level.SEVERE, "Exception: ", e);
			}
		}
	
		/**
		 * This method provides a session object. If session is associated with
		 * sessionFactory it gives same session, if session is closed it gives new
		 * Session
		 * 
		 * @return Session
		 */
	
		public static Session getSession() {
			if (count == 0) {
				buildSessionFactory();
				count++;
			}
			if (!session.isOpen()) {
				session = factory.openSession();
			}
			return session;
		}
	
		/**
		 * This method provides Configuration object associated with SessionFactory,
		 * If sessionFactory not existing, it creates new SessionFacotry
		 * 
		 * @return Configuration
		 */
	
		public static Configuration getConfiguration() {
	
			if (count == 0) {
				buildSessionFactory();
				count++;
			}
	
			return cfg;
	
		}
	}