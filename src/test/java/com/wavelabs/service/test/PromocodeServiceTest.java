package com.wavelabs.service.test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wavelabs.dao.PromoCodeDao;
import com.wavelabs.model.PromoCode;
import com.wavelabs.service.PromocodeService;

@RunWith(MockitoJUnitRunner.class)
public class PromocodeServiceTest {

	@Mock
	PromoCodeDao promoCodeDao;

	@InjectMocks
	PromocodeService promoCodeService;

	@Test
	public void testAddPromoCode() {
		PromoCode promoCode = DataBuilder.getPromoCode();
		when(promoCodeDao.persistPromoCode(any(PromoCode.class))).thenReturn(promoCode);
		PromoCode promoCode1 = promoCodeService.addPromoCode(promoCode);
		Assert.assertEquals(promoCode, promoCode1);
	}
	
	@Test
	public void testGetPromoCode(){
		PromoCode promoCode = DataBuilder.getPromoCode();
		when(promoCodeDao.getPromoCode(anyInt())).thenReturn(promoCode);
		PromoCode promoCode1 = promoCodeService.getPromoCode(1);
		Assert.assertEquals(promoCode, promoCode1);
	}
	
	@Test
	public void testUpdatePromoCode(){
		PromoCode promoCode = DataBuilder.getPromoCode();
		when(promoCodeDao.updatePromoCode(any(PromoCode.class))).thenReturn(promoCode);
		PromoCode promoCode1 = promoCodeService.updatePromoCode(promoCode);
		Assert.assertEquals(promoCode, promoCode1);
	}
}
