package com.wavelabs.service.test;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.wavelabs.model.DiscountType;
import com.wavelabs.model.Member;
import com.wavelabs.model.Message;
import com.wavelabs.model.Plan;
import com.wavelabs.model.PromoCode;
import com.wavelabs.model.Scheme;
import com.wavelabs.model.ZipCode;

public class DataBuilder {
	public static Member getMember() {
		Member member = new Member();
		member.setId(1);
		member.setEmail("mkkasturi@gmail.com");
		member.setAmountPaidForPresentPlan(600);
		member.setFreeTrialExpiryDate(new Date(1403685556000L));
		member.setPlanRenewalDate(new Date(1403685556000L));
		member.setPlan(getPlan());
		member.setNextSubscriptionPlan(getPlan());
		member.setFreeTrialPromoCode(getPromoCode());
		PromoCode promCode1 = new PromoCode();
		PromoCode promCode2 = new PromoCode();
		Set<PromoCode> promoCodes = new HashSet<PromoCode>();
		promoCodes.add(promCode1);
		promoCodes.add(promCode2);
		member.setUsedPromoCodes(promoCodes);
		return member;
	}

	public static Plan getPlan() {
		Plan plan = new Plan();
		plan.setId(1);
		plan.setName("PRO");
		plan.setPrice(1000);
		plan.setDurationInMonths(6);
		plan.setNoOfDaysForFreeTrial(0);
		return plan;
	}
	
	public static Plan getFreeTrial() {
		Plan plan = new Plan();
		plan.setId(1);
		plan.setName("PRO");
		plan.setPrice(0);
		plan.setDurationInMonths(0);
		plan.setNoOfDaysForFreeTrial(10);
		return plan;
	}

	public static PromoCode getPromoCode() {
		PromoCode promoCode = new PromoCode();
		promoCode.setId(1);
		promoCode.setCode("PC100123");
		promoCode.setDiscountAmount(50);
		promoCode.setDiscountType(DiscountType.PERCENTAGE);
		promoCode.setExpiresOn(new Date(1403685556000L));
		promoCode.setValidFrom(new Date(1403685556000L));
		Set<Integer> months = new HashSet<Integer>();
		months.add(3);
		months.add(6);
		promoCode.setNoOfMonthsApplicable(months);
		return promoCode;
	}
	
	public static Scheme getScheme(){
		Scheme scheme = new Scheme();
		scheme.setId(1);
		scheme.setName("Scheme A");
		scheme.setFreeTrial((byte) 1);
		scheme.setIsActive((byte) 0);
		Plan plan1 = new Plan();
		Plan plan2 = new Plan();
		Set<Plan> plans = new HashSet<Plan>();
		plans.add(plan1);
		plans.add(plan2);
		scheme.setPlans(plans);
		return scheme;
	}
	
	public static ZipCode getZipCode(){
		ZipCode zip = new ZipCode();
		zip.setId(1);
		zip.setZipCode("90848");
		zip.setCity("Phoenix");
		zip.setState("Arizona");
		Scheme scheme1 = new Scheme();
		Scheme scheme2 = new Scheme();
		Set<Scheme> schemes = new HashSet<Scheme>();
		schemes.add(scheme1);
		schemes.add(scheme2);
		zip.setSchemes(schemes);
		return zip;
	}
	
	public static Message getMessage(){
		Message message = new Message();
		message.setId(200);
		message.setMessage("Success");
		return message;
	}
}
