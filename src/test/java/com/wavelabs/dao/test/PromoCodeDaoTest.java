package com.wavelabs.dao.test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.wavelabs.model.PromoCode;
import com.wavelabs.service.test.DataBuilder;
import com.wavelabs.utility.Helper;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Helper.class)
public class PromoCodeDaoTest {

	private Session session;

	@Mock
	Transaction transaction;

	@Before
	public void setUp() {
		session = mock(Session.class);
		PowerMockito.mockStatic(Helper.class);
		when(Helper.getSession()).thenReturn(session);
	}

	@Test
	public void testGetPromoCode() {
		PromoCode promoCode = DataBuilder.getPromoCode();
		when(session.get(eq(PromoCode.class), any())).thenReturn(promoCode);
		PromoCode promoCode1 = ObjectBuilder.getPromoCodeDao().getPromoCode(1);
		Assert.assertEquals(promoCode, promoCode1);
	}
	
	@Test
	public void testPersistPromoCode(){
		PromoCode promoCode = DataBuilder.getPromoCode();
		when(session.save(any(PromoCode.class))).thenReturn(123);
		when(session.beginTransaction()).thenReturn(transaction);
		PromoCode promoCode1 = ObjectBuilder.getPromoCodeDao().persistPromoCode(promoCode);
		Assert.assertEquals(promoCode, promoCode1);
	}
	
	@Test
	public void testUpdatePromoCode(){
		PromoCode promoCode = DataBuilder.getPromoCode();
		PowerMockito.doNothing().when(session).saveOrUpdate(any(PromoCode.class));
		when(session.beginTransaction()).thenReturn(transaction);
		PromoCode promoCode1 = ObjectBuilder.getPromoCodeDao().updatePromoCode(promoCode);
		Assert.assertEquals(promoCode, promoCode1);
	}
	
}
