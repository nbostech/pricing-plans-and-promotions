package com.wavelabs.dao.test;

import com.wavelabs.dao.MemberDao;
import com.wavelabs.dao.PlanDao;
import com.wavelabs.dao.PromoCodeDao;
import com.wavelabs.dao.SchemeDao;
import com.wavelabs.dao.ZipCodeDao;

public class ObjectBuilder {
	private static MemberDao memberDao;
	private static PlanDao planDao;
	private static PromoCodeDao promoCodeDao;
	private static SchemeDao schemeDao;
	private static ZipCodeDao zipCodeDao;

	public static MemberDao getMemberDao() {
		if (memberDao == null) {
			memberDao = new MemberDao();
		}
		return memberDao;
	}

	public static PlanDao getPlanDao() {
		if (planDao == null) {
			planDao = new PlanDao();
		}
		return planDao;
	}

	public static PromoCodeDao getPromoCodeDao() {
		if (promoCodeDao == null) {
			promoCodeDao = new PromoCodeDao();
		}
		return promoCodeDao;
	}

	public static SchemeDao getSchemeDao() {
		if (schemeDao == null) {
			schemeDao = new SchemeDao();
		}
		return schemeDao;
	}

	public static ZipCodeDao getZipCodeDao() {
		if (zipCodeDao == null) {
			zipCodeDao = new ZipCodeDao();
		}
		return zipCodeDao;
	}
}
