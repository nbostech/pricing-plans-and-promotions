package com.wavelabs.dao.test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.wavelabs.model.Plan;
import com.wavelabs.model.Scheme;
import com.wavelabs.service.SchemeService;
import com.wavelabs.service.test.DataBuilder;
import com.wavelabs.utility.Helper;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Helper.class)
public class PlanDaoTest {
	private Session session;

	@Mock
	Transaction transaction;

	@Mock
	SchemeService schemeService;

	@Before
	public void setUp() {
		session = mock(Session.class);
		PowerMockito.mockStatic(Helper.class);
		when(Helper.getSession()).thenReturn(session);
	}

	@Test
	public void testGetPlan() {
		Plan plan = DataBuilder.getPlan();
		when(session.get(eq(Plan.class), any())).thenReturn(plan);
		Plan plan1 = ObjectBuilder.getPlanDao().getPlan(1);
		Assert.assertEquals(plan, plan1);
	}

	/*@Test
	public void testPersistPlan() {
		Plan plan = DataBuilder.getPlan();
		Scheme scheme = DataBuilder.getScheme();
		when(schemeService.getScheme(anyInt())).thenReturn(scheme);
		when(session.save(any(Scheme.class))).thenReturn(123);
		when(session.beginTransaction()).thenReturn(transaction);
		Plan plan1 = ObjectBuilder.getPlanDao().persistPlan(plan, anyInt());
		Assert.assertEquals(plan, plan1);
	}*/

	/*
	 * @Test public void testDeletePlan(){ Plan plan = DataBuilder.getPlan();
	 * when(session.get(eq(Plan.class), any())).thenReturn(plan);
	 * PowerMockito.doNothing().when(session).delete(any(Plan.class));
	 * when(session.beginTransaction()).thenReturn(transaction);
	 * PowerMockito.doNothing().when(ObjectBuilder.getPlanDao()).deletePlan(1);
	 * }
	 */

}
