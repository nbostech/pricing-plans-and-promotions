package com.wavelabs.model.test;

import org.junit.Assert;
import org.junit.Test;

import com.wavelabs.model.Plan;

public class PlanTest {

	private static final double DELTA = 1e-15;

	@Test
	public void testGetId() {
		Plan plan = new Plan();
		plan.setId(12);
		Assert.assertEquals(12, plan.getId());
	}

	@Test
	public void testGetName() {
		Plan plan = new Plan();
		plan.setName("PRO");
		Assert.assertEquals("PRO", plan.getName());
	}

	@Test
	public void testGetDurationInMonths() {
		Plan plan = new Plan();
		plan.setDurationInMonths(6);
		Assert.assertEquals(6, plan.getDurationInMonths());
	}

	@Test
	public void testGetNoOfDaysForFreeTrial() {
		Plan plan = new Plan();
		plan.setNoOfDaysForFreeTrial(10);
		Assert.assertEquals(10, plan.getNoOfDaysForFreeTrial());
	}

	@Test
	public void testGetPrice() {
		Plan plan = new Plan();
		plan.setPrice(544.5);
		Assert.assertEquals(544.5, plan.getPrice(), DELTA);
	}
}
