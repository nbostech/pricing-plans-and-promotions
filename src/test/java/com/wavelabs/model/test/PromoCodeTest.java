package com.wavelabs.model.test;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import com.wavelabs.model.DiscountType;
import com.wavelabs.model.PromoCode;


public class PromoCodeTest {
	
	private static final double DELTA = 1e-15;

	@Test
	public void testGetId() {
		PromoCode promoCode = new PromoCode();
		promoCode.setId(12);
		Assert.assertEquals(12, promoCode.getId());
	}
	
	@Test
	public void testGetCode(){
		PromoCode promoCode = new PromoCode();
		promoCode.setCode("PC100123");
		Assert.assertEquals("PC100123", promoCode.getCode());
	}

	@Test
	public void testValidFrom(){
		PromoCode promoCode = new PromoCode();
		promoCode.setValidFrom(new Date(1403685556000L));
		Assert.assertEquals(new Date(1403685556000L), promoCode.getValidFrom());
	}
	
	@Test
	public void testExpiresOn(){
		PromoCode promoCode = new PromoCode();
		promoCode.setExpiresOn((new Date(1403685556000L)));
		Assert.assertEquals(new Date(1403685556000L), promoCode.getExpiresOn());
	}
	
	@Test
	public void testDiscountType(){
		PromoCode promoCode = new PromoCode();
		promoCode.setDiscountType(DiscountType.PERCENTAGE);
		Assert.assertEquals(DiscountType.PERCENTAGE, promoCode.getDiscountType());
	}
	
	@Test
	public void testGetNoOfMonthsApplicable(){
		PromoCode promoCode = new PromoCode();
		Set<Integer> months = new HashSet<Integer>();
		promoCode.setNoOfMonthsApplicable(months);
		Assert.assertEquals(months, promoCode.getNoOfMonthsApplicable());
	}
	
	@Test
	public void testDiscountAmount(){
		PromoCode promoCode = new PromoCode();
		promoCode.setDiscountAmount(20);
		Assert.assertEquals(20, promoCode.getDiscountAmount(), DELTA);
	}
}
