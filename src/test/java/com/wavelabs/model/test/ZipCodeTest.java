package com.wavelabs.model.test;

import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import com.wavelabs.model.Scheme;
import com.wavelabs.model.ZipCode;

public class ZipCodeTest {
	@Test
	public void testGetId() {
		ZipCode zip = new ZipCode();
		zip.setId(25);
		Assert.assertEquals(25, zip.getId());
	}

	@Test
	public void testGetZipCode() {
		ZipCode zip = new ZipCode();
		zip.setZipCode("87654");
		Assert.assertEquals("87654", zip.getZipCode());
	}

	@Test
	public void testGetCity() {
		ZipCode zip = new ZipCode();
		zip.setCity("Phoenix");
		Assert.assertEquals("Phoenix", zip.getCity());
	}

	@Test
	public void testGetState() {
		ZipCode zip = new ZipCode();
		zip.setState("Arizona");
		Assert.assertEquals("Arizona", zip.getState());
	}

	@Test
	public void testGetSchemes() {
		ZipCode zip = new ZipCode();
		Scheme scheme1 = new Scheme();
		Scheme scheme2 = new Scheme();
		Scheme scheme3 = new Scheme();
		Scheme scheme4 = new Scheme();
		Set<Scheme> schemes = new HashSet<Scheme>();
		schemes.add(scheme1);
		schemes.add(scheme2);
		schemes.add(scheme3);
		schemes.add(scheme4);
		zip.setSchemes(schemes);
		Assert.assertEquals(schemes, zip.getSchemes());

	}
}
