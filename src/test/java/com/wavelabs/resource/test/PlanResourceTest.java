package com.wavelabs.resource.test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import com.wavelabs.model.Plan;
import com.wavelabs.resource.PlanResource;
import com.wavelabs.service.PlanService;
import com.wavelabs.service.test.DataBuilder;

@RunWith(MockitoJUnitRunner.class)
public class PlanResourceTest {
	
	@Mock
	PlanService planService;
	
	@InjectMocks
	PlanResource planResource;
	
	@Test
	public void testAddPlan(){
		Plan plan = DataBuilder.getPlan();
		when(planService.addPlan(any(Plan.class), anyInt())).thenReturn(plan);
		ResponseEntity<Plan> entity = planResource.addPlan(plan, 1);
		Assert.assertEquals(200, entity.getStatusCodeValue());
	}
}
