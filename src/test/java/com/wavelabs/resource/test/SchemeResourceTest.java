package com.wavelabs.resource.test;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import com.wavelabs.model.Scheme;
import com.wavelabs.resource.SchemeResource;
import com.wavelabs.service.SchemeService;
import com.wavelabs.service.test.DataBuilder;

@RunWith(MockitoJUnitRunner.class)
public class SchemeResourceTest {
	@Mock
	SchemeService schemeService;

	@InjectMocks
	SchemeResource schemeResource;
	
	@Test
	public void testAddScheme(){
		Scheme scheme = DataBuilder.getScheme();
		when(schemeService.addScheme(any(Scheme.class))).thenReturn(scheme);
		ResponseEntity<Scheme> entity = schemeResource.addScheme(scheme);
		Assert.assertEquals(200, entity.getStatusCodeValue());
	}
	
}
