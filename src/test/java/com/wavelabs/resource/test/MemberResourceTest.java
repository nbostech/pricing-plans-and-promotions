package com.wavelabs.resource.test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import com.wavelabs.model.Member;
import com.wavelabs.resource.MemberResource;
import com.wavelabs.service.MemberService;
import com.wavelabs.service.test.DataBuilder;

@RunWith(MockitoJUnitRunner.class)
public class MemberResourceTest {

	@Mock
	MemberService memberService;
	
	@InjectMocks
	MemberResource memberResource;
	
	@Test
	public void testAddMember(){
		Member member =  DataBuilder.getMember();
		when(memberService.addMember(any(Member.class))).thenReturn(member);
		ResponseEntity<Member> entity = memberResource.addMember(member);
		Assert.assertEquals(200, entity.getStatusCodeValue());
	}
	
	@Test
	public void testGetMember1(){
		Member member = DataBuilder.getMember();
		when(memberService.getMember(anyInt())).thenReturn(member);
		ResponseEntity<Object> entity = memberResource.getMember(1);
		Assert.assertEquals(200, entity.getStatusCodeValue());
	}
	
	@Test
	public void testGetMember2(){
		when(memberService.getMember(anyInt())).thenReturn(null);
		ResponseEntity<Object> entity = memberResource.getMember(1);
		Assert.assertEquals(404, entity.getStatusCodeValue());
	}

}
